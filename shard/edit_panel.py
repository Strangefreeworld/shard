"""This defines the class that is the text edit window"""

from pathlib import Path
import logging
import wx
import wx.html2
import wx.stc as stc
from converters import converter_data, converter_function

NOT_FOUND_TUPLE = (-1, -1)


class EditPanel(wx.Panel):
    def __init__(self, parent, *args, **kw):
        super().__init__(parent, *args, **kw)
        self._sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self._sizer)
        self._edit = stc.StyledTextCtrl(self, 1, style=wx.TE_MULTILINE)
        self._edit.SetWrapMode(wx.stc.STC_WRAP_WORD)
        self._sizer.Add(self._edit, -1, wx.EXPAND)
        self._full_path = None
        self._converter_format = "Markdown"
        self._converter_function = converter_function("Markdown")
        self._preview_panel = None
        self._show_line_numbers = False
        self._last_render = None
        self._saved = False

    def _init_margin(self):
        self._edit.SetViewWhiteSpace(False)
        self._edit.SetMargins(5, 0)
        self._edit.SetMarginType(1, stc.STC_MARGIN_NUMBER)
        self._edit.SetMarginWidth(1, 0)

    def toggle_line_numbers(self):
        self._show_line_numbers = not self._show_line_numbers
        if self._show_line_numbers:
            self._edit.SetMarginWidth(1, 0)
        else:
            self._edit.SetMarginWidth(1, 35)

    @property
    def full_path(self):
        return self._full_path

    @full_path.setter
    def full_path(self, full_path):
        self._full_path = full_path

    def title(self):
        return Path(self._full_path).name if self._full_path else "Untitled"

    @property
    def directory(self):
        return Path(self._full_path).parent if self._full_path else Path.cwd()

    def preview_panel(self, panel):
        self._preview_panel = panel

    @property
    def modified(self):
        return self._edit.IsModified()

    @property
    def empty(self):
        return self._edit.GetValue() == ""

    @property
    def location(self):
        current_line = self._edit.GetCurrentLine() + 1
        total_lines = self._edit.GetLineCount()
        column = self._edit.GetColumn(self._edit.GetCurrentPos())
        return f"Ln: {current_line}/{total_lines} Col: {column}"

    def _render(self):
        if not self._preview_panel:
            logging.warning("No preview panel has been set")
            return None
        if self._converter_function:
            self._last_render = self._edit.GetValue()
            self._preview_panel.set_html(self._converter_function(self._last_render))

    def update_preview(self):
        if self._edit.GetValue() != self._last_render:
            self._render()

    def new_file(self, text_format="Markdown"):
        self._full_path = None
        self._saved = True
        self._last_render = ""
        self._edit.SetValue("")
        self._converter_format = text_format
        self._converter_function = converter_function(text_format)
        self._render()

    def find_text(self, find_data):
        start_pos = self._edit.GetCurrentPos()
        max_len = len(self._edit.GetValue())
        data_found = self._edit.FindText(
            start_pos, max_len, find_data.GetFindString(), find_data.GetFlags()
        )
        if data_found != NOT_FOUND_TUPLE:
            self._edit.GotoPos(data_found[0])
            self._edit.SetSelection(data_found[0], data_found[1])
            return True
        return False

    def save_file(self, filename=None):
        if self._full_path is None and filename is None:
            raise FileNotFoundError
        if filename is not None:
            self._edit.SaveFile(filename)
            self._full_path = filename
        else:
            self._edit.SaveFile(self._full_path)

    def load_file(self, filename):
        self._full_path = filename
        self._edit.LoadFile(filename)
        self._saved = True
        format_data = converter_data(Path(filename).suffix)
        self._converter_format = format_data.name
        self._converter_function = format_data.function
        self.update_preview()

    @property
    def converter(self):
        return self._converter_format

    @property
    def current_line(self):
        return self._edit.GetCurrentLine() + 1

    @current_line.setter
    def current_line(self, line):
        self._edit.GotoLine(line)

    @property
    def total_lines(self):
        return self._edit.GetLineCount()
