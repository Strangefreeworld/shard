"""This implements a simple preview editor for Markdown-style formats"""

# pylint: disable=E1101

import argparse
import logging
from pathlib import Path
import click
import wx
import wx.html2
import wx.stc
import toml
from gotoline import GoToLineDialog
from converters import add_asciidoc
from edit_panel import EditPanel
from html_panel import HtmlPanel
import app_config

class ShardFrame(wx.Frame):
    def __init__(self, parent):
        wx.Frame.__init__(self, parent, title="Shard", size=(800, 600))
        self._setup_menu_bar()
        self.splitter = wx.SplitterWindow(self, -1)
        self.splitter.SetMinimumPaneSize(20)
        self._text = EditPanel(self.splitter)
        self._preview = HtmlPanel(self.splitter)
        self.splitter.SplitVertically(self._text, self._preview)
        self._text.preview_panel(self._preview)
        self._setup_timer()
        self._bind_commands()
        self.timer.Start(300)
        self.SetName("Shard")
        self.ShowFullScreen(False)
        self.status_bar = self.CreateStatusBar(4)
        self.status_bar.SetStatusWidths([200, 200, 125, 200])
#        self.SetIcon(wx.Icon("excalibur.png"))
        self.filename = None
        self.find_data = wx.FindReplaceData()
        self._load_config()
        self.status_bar.SetStatusText(f"{self._text.converter} Converter", 1)
        self.Show()

    def _setup_menu_bar(self):
        self.menu_bar = wx.MenuBar()
        self.file_menu = wx.Menu()
        self.menu_newfile = self.file_menu.Append(wx.ID_NEW)
        self.menu_fileopen = self.file_menu.Append(wx.ID_OPEN)
        self.menu_filesave = self.file_menu.Append(wx.ID_SAVE)
        self.menu_filesaveas = self.file_menu.Append(wx.ID_SAVEAS)
        self.file_menu.AppendSeparator()
        self.file_menu.Append(wx.ID_EXIT)
        self.menu_bar.Append(self.file_menu, "&File")
        self.edit_menu = wx.Menu()
        self.menu_editundo = self.edit_menu.Append(wx.ID_UNDO)
        self.menu_editredo = self.edit_menu.Append(wx.ID_REDO)
        self.edit_menu.AppendSeparator()
        self.menu_editcut = self.edit_menu.Append(wx.ID_CUT)
        self.menu_editcopy = self.edit_menu.Append(wx.ID_COPY)
        self.menu_editpaste = self.edit_menu.Append(wx.ID_PASTE)
        self.edit_menu.AppendSeparator()
        self.menu_selectall = self.edit_menu.Append(wx.ID_SELECTALL)
        self.menu_bar.Append(self.edit_menu, "&Edit")
        self.search_menu = wx.Menu()
        self.menu_find = self.search_menu.Append(wx.ID_FIND)
        self.menu_replace = self.search_menu.Append(wx.ID_REPLACE)
        self.menu_goto = self.search_menu.Append(
            wx.ID_ANY, "&Goto Line...\tCtrl+G", "Goto Line Number"
        )
        self.menu_bar.Append(self.search_menu, "Search")
        self.view_menu = wx.Menu()
        self.view_toggle_numbers = self.view_menu.Append(
            wx.ID_ANY,
            "Toggle &Line Numbers\tAlt+Shift+L",
            "Enable/Disable line numbers",
            wx.ITEM_CHECK,
        )
        self.menu_bar.Append(self.view_menu, "&View")
        self.SetMenuBar(self.menu_bar)

    def _setup_timer(self):
        self.timer = wx.Timer(self)

    def _load_config(self):
        try:
            config_file = app_config.config_file_path("shard.toml")
            with open(config_file, "r") as config_data:
                self.config = toml.load(config_data)
            if self.config["asciidoc_disabled"]:
                if "Asciidoc" in self.config["converter_formats"]:
                    self.config["converter_formats"].remove("Asciidoc")
            else:
                add_asciidoc()
        except FileNotFoundError:
            wx.MessageBox(
                "Config file not found", "Config File Error", wx.OK | wx.ICON_ERROR
            )
            self.Close(True)
        except toml.TomlDecodeError as e:
            wx.MessageBox(
                "Config file not valid", "Config File Error", wx.OK | wx.ICON_ERROR
            )
            self.Close(True)

    def toggle_line_numbers(self, e):
        self._text.toggle_line_numbers()

    def _bind_commands(self):
        self.Bind(wx.EVT_MENU, self.do_exit, id=wx.ID_EXIT)
        self.Bind(wx.EVT_MENU, self.file_new, id=wx.ID_NEW)
        self.Bind(wx.EVT_MENU, self.file_open, id=wx.ID_OPEN)
        self.Bind(wx.EVT_MENU, self.file_save, id=wx.ID_SAVE)
        self.Bind(wx.EVT_MENU, self.file_saveas, id=wx.ID_SAVEAS)
        self.Bind(wx.EVT_MENU, self.show_find, id=wx.ID_FIND)
        self.Bind(wx.EVT_MENU, self.show_replace, id=wx.ID_REPLACE)
        self.Bind(wx.EVT_MENU, self.toggle_line_numbers, self.view_toggle_numbers)
        self.Bind(wx.EVT_MENU, self.goto_line, self.menu_goto)
        self.Bind(wx.EVT_TIMER, self.timer_update)
        self.Bind(wx.EVT_CLOSE, self.window_close)

    def window_close(self, _):
        if self.close_panel():
            self.Destroy()

    def do_exit(self, _):
        if self.close_panel():
            self.Close(True)

    def timer_update(self, _):
        self.status_bar.SetStatusText(self._text.location)
        dirty_status = "Modified" if self._text.modified else ""
        self.status_bar.SetStatusText(dirty_status, 2)
        self._text.update_preview()

    def goto_line(self, _):
        with GoToLineDialog(
            self,
            "Go to Line",
            self._text.total_lines,
            self._text.current_line + 1,
        ) as dialog:
            dialog_result = dialog.ShowModal()
            if dialog_result == wx.ID_OK:
                self._text.current_line = dialog.result

    def file_new(self, event):
        if self.close_panel():
            result = wx.GetSingleChoice(
                "Available formats", "Select a format", self.config["converter_formats"]
            )
            logging.info("Creating new file")
            self._text.new_file(result)
            self.SetTitle("Shard")
            self.status_bar.SetStatusText(f"{self._text.converter} Converter", 1)

    def _generate_preview(self):
        self._text.update_preview()

    def open(self, filename):
        logging.info(f"Opening file {filename}")
        self._text.load_file(filename)
        self.SetTitle(f"Shard - {self._text.title()}")
        self.status_bar.SetStatusText(f"{self._text.converter} Converter", 1)

    def file_open(self, _):
        if self.close_panel():
            with wx.FileDialog(
                self,
                "Open File",
                wildcard="Markdown files (*.md)|*.md|All Files|*",
                style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST,
            ) as dialog:
                if dialog.ShowModal() == wx.ID_CANCEL:
                    return

                self.open(dialog.GetPath())

    def _save(self):
        if self._text.title() != "Untitled":
            self._text.save_file()
        else:
            self._saveas()

    def bind_find_events(self, dlg):
        dlg.Bind(wx.EVT_FIND, self._find_text)
        dlg.Bind(wx.EVT_FIND_NEXT, self._find_text)
        dlg.Bind(wx.EVT_FIND_REPLACE, self._replace_text)
        dlg.Bind(wx.EVT_FIND_REPLACE_ALL, self._replace_all)
        dlg.Bind(wx.EVT_FIND_CLOSE, self._close_find)

    def _find_text(self, _):
        found = self._text.find_text(self.find_data)
        if not found:
            self.status_bar.SetStatusText("Text not found", 3)

    def _replace_text(self, _):
        pass

    def _replace_all(self, _):
        pass

    def _close_find(self, event):
        event.GetDialog().Destroy()

    def show_find(self, _):
        dlg = wx.FindReplaceDialog(self, self.find_data, "Find")
        self.bind_find_events(dlg)
        dlg.Show(True)

    def show_replace(self, _):
        dlg = wx.FindReplaceDialog(
            self, self.find_data, "Find and Replace", wx.FR_REPLACEDIALOG
        )
        self.bind_find_events(dlg)
        dlg.Show(True)

    def _saveas(self):
        with wx.FileDialog(
            self,
            "Save File As...",
            str(Path.cwd()),
            "",
            "*.*",
            wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT,
        ) as dialog:
            if dialog.ShowModal() == wx.ID_OK:
                self._text.save_file(dialog.GetPath())
                self.SetTitle(f"Shard - {self._text.title()}")

    def file_save(self, _):
        self._save()

    def file_saveas(self, _):
        self._saveas()

    def close_panel(self):
        if self._text.modified:
            check_save = wx.MessageBox(
                "Text has been modified. Save changes?",
                "Text Modified",
                wx.YES_NO | wx.CANCEL | wx.ICON_QUESTION,
            )
            if check_save != wx.CANCEL:
                if check_save == wx.YES:
                    self._save()
                return True
            return False
        return True


@click.command()
@click.argument("files", nargs=-1, type=click.Path())
def run_shard(files):
    app_config.config_dir_files(["shard.toml"])
    logging.basicConfig(
        filename=app_config.config_file_path("editor.log"),
        level=logging.INFO,
        format="%(asctime)s %(levelname)s %(threadName)s %(name)s %(message)s",
    )
    app = wx.App(False)
    frame = ShardFrame(None)
    logging.info("App started")
    if files:
        frame.open(files[0])
    app.MainLoop()
