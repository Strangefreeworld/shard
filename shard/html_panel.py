"""This file defines the class used to preview the HTML of the text input"""
import wx
import wx.html2


class HtmlPanel(wx.Panel):
    def __init__(self, parent, *args, **kw):
        super().__init__(parent, *args, **kw)
        self._sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self._sizer)
        self._preview = wx.html2.WebView.New(self)
        self._sizer.Add(self._preview, -1, wx.EXPAND)

    def set_html(self, html_text):
        self._preview.SetPage(html_text, "")
