"""This file defines the GoToLineDialog"""

import wx

class GoToLineDialog(wx.Dialog):
    """
    This class implements the GoTo Line dialog
    """

    def __init__(self, parent, title, max_lines, current_line):
        super(GoToLineDialog, self).__init__(parent, title=title)
        sizer = wx.BoxSizer(wx.VERTICAL)
        label = wx.StaticText(self, -1, "Select Line", (45, 15))
        sizer.Add(label, 0, wx.ALIGN_CENTRE | wx.ALL)
        self.spin_ctrl = wx.SpinCtrl(self, -1, "Line:", (70, 50))
        self.spin_ctrl.SetRange(1, max_lines)
        self.spin_ctrl.SetValue(current_line)
        sizer.Add(self.spin_ctrl, 1, wx.ALIGN_CENTRE | wx.ALL)
        self._result = current_line
        btnsizer = wx.StdDialogButtonSizer()
        btn = wx.Button(self, wx.ID_OK)
        btn.SetHelpText("The OK button completes the dialog")
        btn.SetDefault()
        btnsizer.AddButton(btn)

        btn = wx.Button(self, wx.ID_CANCEL)
        btn.SetHelpText("The Cancel button cancels the dialog. (Cool, huh?)")
        btnsizer.AddButton(btn)
        btnsizer.Realize()

        sizer.Add(btnsizer, 0, wx.ALL, 5)
        self.Bind(wx.EVT_SPINCTRL, self.on_spin, self.spin_ctrl)
        self.Bind(wx.EVT_TEXT, self.on_text, self.spin_ctrl)

        self.SetSizer(sizer)
        sizer.Fit(self)

    def on_spin(self, event):
        self._result = self.spin_ctrl.GetValue()

    def on_text(self, event):
        self._result = self.spin_ctrl.GetValue()

    @property
    def result(self):
        return self._result
